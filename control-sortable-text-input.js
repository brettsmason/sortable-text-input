( function( $, api ) {

    /* === Checkbox Multiple Control === */

    api.controlConstructor['sortable-text-input'] = api.Control.extend( {
        ready: function() {
            var control = this;

            $( '.sortable-text-input-list' ).sortable({
                handle: '.sortable-text-input-handle',
                axis: 'y',
				update: function( e, ui ) {
					$( '.sortable-text-input-item' ).trigger( 'change' );
				}
            });

            $( '.sortable-text-input-item', control.container ).change(
                function() {

                    /* Get the value, and convert to string. */
					input_values = $( this ).parents( '.sortable-text-input-list' ).find( '.sortable-text-input-item' ).map( function() {
						var value = $(this).val();
						if (value) {
							return this.name + ',' + value;
						}
					}).get().join( '|' );

					// Add inputs and their values to our hidden field
					$( this ).parents( '.sortable-text-input-list' ).find( '.sortable-text-input-values' ).val( input_values ).trigger( 'change' );

                    // Set the value.
                    if ( null === input_values ) {
                        control.setting.set( '' );
                    } else {
                        control.setting.set( input_values );
                    }
                }
            );
        }
    } );

} )( jQuery, wp.customize );
