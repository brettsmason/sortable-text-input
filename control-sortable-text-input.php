<?php
/**
 * Sortable text input customize control class.
 *
 * @access public
 */
class Sortable_Text_Input extends WP_Customize_Control {
	/**
	 * The type of customize control being rendered.
	 *
	 * @access public
	 * @var    string
	 */
	public $type = 'sortable-text-input';
	/**
	 * Enqueue scripts/styles.
	 *
	 * @access public
	 * @return void
	 */
	public function enqueue() {
		wp_enqueue_script( 'tiptoe-sortable-input' );
	}
	/**
	 * Add custom parameters to pass to the JS via JSON.
	 *
	 * @access public
	 * @return void
	 */
	public function to_json() {
		parent::to_json();
		$this->json['value']   = ! is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
		$this->json['choices'] = $this->choices;
		$this->json['link']    = $this->get_link();
		$this->json['id']      = $this->id;
	}
	/**
	 * Underscore JS template to handle the control's output.
	 *
	 * @access public
	 * @return void
	 */
	public function content_template() { ?>

		<# if ( ! data.choices ) {
			return;
		} #>

		<# if ( data.label ) { #>
			<span class="customize-control-title">{{ data.label }}</span>
		<# } #>

		<# if ( data.description ) { #>
			<span class="description customize-control-description">{{{ data.description }}}</span>
		<# } #>

		<ul class="sortable-text-input-list">
			<# _.each( data.choices, function( label, choice ) { #>
				<li class="sortable-text-input-list-item">
					<i class="dashicons dashicons-menu sortable-text-input-handle"></i>
					<label>
						{{ label }}
						<input id="{{ choice }}" class="sortable-text-input-item" name="{{ choice }}" type="text" />
					</label>
				</li>
			<# } ) #>
		</ul>

		<input type="text" class="sortable-text-input-values" value="{{ data.value }}" />
	<?php }
}
